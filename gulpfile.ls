gulp = require 'gulp'
gulp-load-plugins = require 'gulp-load-plugins'
$ = gulp-load-plugins!

gulp.task 'views', ->
    return gulp.src(['./*.jade', '!app/layout.jade'])
        .pipe($.plumber())
        .pipe($.jade({pretty: true}))
        .pipe gulp.dest '.'


gulp.task 'styles', ->
    return gulp.src('./styles/main.styl')
        .pipe($.plumber())
        .pipe(gulp.dest('./styles'))
        .pipe($.size())

gulp.task 'scripts', ->
    return gulp.src('./scripts/**/*.ls')
        .pipe($.plumber())
        .pipe($.livescript())
        .pipe(gulp.dest('./scripts'))

/*
  saved comment
*/
gulp.task \watch, ->
  #server = $.livereload!
  $.livereload.listen!
  gulp.watch \*
    .on \change, $.livereload.changed

  gulp.watch \./*.jade, [ \views ]
  gulp.watch \./**/*.ls, [ \scripts ]
  gulp.watch \./**/*.styl, [ \styles ]
